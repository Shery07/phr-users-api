const mongoose = require("mongoose");
const express = require("express");
const home = require("./routes/home");
const users = require("./routes/users");
const login = require("./routes/login");
const app = express();

//mongodb connection
mongoose
  .connect("mongodb://localhost/PHRSystem", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true 
  })
  .then(() => console.log("Connected to MongoDB"))
  .catch(err => console.error("Could not connect to MongoDB"));

//build-in middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("public"));

//Setting up template engine
app.set("view engine", "hbs");

//default root for the application to access templates
app.set("views", "./views"); 

//Setting Routes
app.use("/", home);
app.use("/users", users);
app.use("/login", login);


//configuring port
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));





